import requests
import sys
import json
import datetime
import os


# TODO: tweak it so it checks a file/list for downloaded posts and ignore those
# for the final rpan download list so we only generate a list of streams that
# have not been downloaded yet so we can cron this

# STEP 1: GET ALL POSTS

headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

def get_results(all_posts, username, options, count):
    url = f"https://www.reddit.com/user/{username}.json{options}"
    response = requests.get(url, headers=headers)
    # print(f'len of response is {len(response.content)}')

    data = response.json()

    for child in data['data']['children']:
        all_posts.append(child)
    after = data['data']['after']

    if(after != None):
        count += 25
        options = f"?count={count}&after={after}"
        # print(options)
        get_results(all_posts, username, options, count)  # loop
    # else:
    #     with open('all_posts.json','w') as f:
    #         json.dump(all_posts,f)
    else:
        print("step 1 done")

def get_archive_list(username):
    all_posts = []

    get_results(all_posts, username,"", 0)

    # STEP 2: FILTER ALL POSTS TO GET RPAN STREAM INFO AND GENERATE FILENAME
    stream_list = []

    for post in all_posts:
        data = post['data']
        # print(f"trying {data['name']}")
        try:
            flair_text = data['link_flair_text']
        except:
            flair_text = ""
        is_stream = True if (flair_text == "Broadcast") else False
        if is_stream == False:
            # print('Skipping, not a broadcast')
            continue
        created_utc = data['created_utc']
        permalink = data['permalink']
        created_ts = datetime.datetime.fromtimestamp(
            created_utc, datetime.timezone.utc).strftime('%Y-%m-%dT%H.%M.%S%Z')
        filename = f"{created_ts}_{permalink[permalink.rfind('/',0,permalink.rfind('/')):].replace('/','')}.mp4"

        # dunno if this is necessary
        try:
            stream_url = data['rpan_video']['hls_url']
            thumbnail_url = data['rpan_video']['scrubber_media_url']
        except:
            stream_url = ""
            thumbnail_url = ""

        # print(filename)
        append_this = {
            "username" : username,
            "title": data['title'],
            "permalink": 'https://www.reddit.com/'+ permalink,
            "created_utc": created_utc,
            "created_utc_timestamp": created_ts,
            "filename": filename,
            "stream_url": stream_url
            # "downloaded": False
        }

        stream_list.append(append_this)

    print(f"Found {len(stream_list)} streams...")

    # save it so the download is later
    filename = f"rpan_list_{username}.json"
    with open(f'data/{filename}', 'w') as f:
        json.dump(stream_list, f)

    print(f"Saved {len(stream_list)} streams to {filename}")


usernames =['eponamom','toyya','singthedamnsong']

for username in usernames:
    get_archive_list(username)
