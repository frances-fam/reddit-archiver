import os
import json
import datetime
import sqlite3

# json_filename = 'data/rpan_list_mini.json'
json_filename ='data/rpan_list.json'


## this also checks if it already exists in the sqlite db...

with open(json_filename, 'r') as f:
    rpan_list = json.load(f)

file_dir = "/mnt/n/all_media/frances-archive/"

dbcon =  sqlite3.connect('database.sqlite')
dbcur = dbcon.cursor()

def download_file(item):
    file_location = file_dir + item['filename']
    # this is without any options aka default settings, may need to tweak it in the future
    cmd = f"""ffmpeg -i {item['stream_url']} -bsf:a aac_adtstoasc -c copy {file_location}"""
    # print(cmd)
    # return

    try:
        os.system(cmd)
        # print('lets pretend')
    except:
        print(f"Something went wrong attempting download of {item['stream_url']}")
        return
    item['is_downloaded'] =1
    update_file(item)

def check_file(item):
    select_query = f"""SELECT stream_url, is_downloaded, is_uploaded as rows FROM video_list WHERE stream_url = '{item["stream_url"]}';"""
    res =  dbcur.execute(select_query)
    result = res.fetchone()
    if result is None:
        print('Stream not in db, adding...')
        insert_query = f"""INSERT INTO video_list 
                    (title, permalink, created_utc, created_utc_timestamp, filename,stream_url,is_downloaded,is_uploaded) VALUES 
                    ('{item["title"]}','{item["permalink"]}','{item["created_utc"]}','{item["created_utc_timestamp"]}','{item["filename"]}','{item["stream_url"]}',0,0);"""
        dbcur.execute(insert_query)
        dbcon.commit()
        item['is_downloaded'] = 0
        item['is_uploaded'] = 0
    else:
        print('found record')
        item['is_downloaded'] = result[1]
        item['is_uploaded'] = result[2]

def update_file(item):
    update_query= f"""UPDATE video_list set is_downloaded=1 where stream_url = '{item["stream_url"]}';"""
    dbcur.execute(update_query)
    dbcon.commit()


for item in rpan_list:
    check_file(item)
    if item['is_downloaded'] == 0:
        download_file(item)
        # print('to download')
    else:
        print('downloaded, skipping')
    # exit()