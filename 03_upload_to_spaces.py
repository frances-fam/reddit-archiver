import os
import dotenv
dotenv.load_dotenv()
import boto3
import sqlite3

session = boto3.session.Session()
client = session.client('s3',
                        region_name='sfo3',
                        endpoint_url='https://sfo3.digitaloceanspaces.com',
                        aws_access_key_id=os.environ.get('SPACES_KEY'),
                        aws_secret_access_key=os.environ.get('SPACES_SECRET'))

file_dir = "/mnt/n/all_media/frances-archive/"

bucket_nm = 'frances-and-family-archive'
s3_folder = 'videos/'

# print(os.environ.get('SPACES_KEY'))

dbcon = sqlite3.connect('database.sqlite')
dbcur = dbcon.cursor()

res = dbcur.execute(f"""select filename, file_size
                    from video_list where is_downloaded=1;
                    """).fetchall()

def upload_file(file_location,filename):
    try:
        client.upload_file(file_location,'frances-and-family-archive',s3_folder+filename,ExtraArgs={'ACL':'public-read'})
        dbcur.execute(f"""update video_list set is_uploaded=1 where filename='{filename}';""")
        dbcon.commit()
    except:
        print(f"""Something went wrong with the upload of {filename}, flagging for redownload""")
        dbcur.execute(f"""update video_list set is_downloaded=0, is_uploaded=0 where filename='{filename}';""")
        dbcon.commit()

for row in res:
    filename = row[0]
    file_size_ori = row[1]
    file_location = file_dir+filename
    print(file_location)
    fileinfo = client.head_object(Bucket=bucket_nm,Key= s3_folder+filename)
    file_size_s3 = fileinfo['ContentLength']
    if file_size_ori != file_size_s3:
        upload_file(file_location,filename)
    else:
        print('File size matches, ignoring')
    

    

    