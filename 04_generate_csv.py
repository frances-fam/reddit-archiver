import json
import csv
import sqlite3
import datetime

# file = './data/rpan_list_2022-08-06.json'

# with open(file,'r') as f:
#     streams = json.load(f)
    
dbcon = sqlite3.connect('database.sqlite')
dbcur = dbcon.cursor()

streams = []
endpoint = 'https://frances-and-family-archive.sfo3.digitaloceanspaces.com/videos/'

res = dbcur.execute(f"""
                  select title, permalink, created_utc, length, filename, is_downloaded, is_uploaded
                  , length_issue
                  from video_list;
                  """).fetchall()

for row in res:
    title_url = f"""`{row[0].replace('&amp;','&')} <https://www.reddit.com{row[1]}>`__"""
    download_url = f"""`download link{"*" if row[7]==1 else ""} <{endpoint}{row[4]}>`__"""
    created_utc = row[2]
    created_ts = datetime.datetime.fromtimestamp(
        created_utc, datetime.timezone.utc).strftime('%Y-%m-%d %I:%M:%S %p %Z')
    is_downloaded=row[5]
    is_uploaded=row[6]
    streams.append({
        'title': title_url,
        'created_utc_timestamp': created_ts,
        'length': row[3],
        'download_url': download_url if is_uploaded==1 else 'tbd',
    })

# get headers
headers = {
    'title': 'Stream Title',
    'created_utc_timestamp' : 'Date Streamed',
    'length' : 'Length',
    'download_url' : 'Download Link'
    }

csv_output = []
# csv_loc = '/mnt/n/all_media/frances-archive/archived-streams.csv'
csv_loc = 'data/{filename}.json'

with open(csv_loc,'w',encoding='utf-8',newline='\n') as f:
    writer = csv.DictWriter(f,delimiter=',',fieldnames = headers.keys() ,quoting=csv.QUOTE_NONNUMERIC)
    writer.writeheader()
    for stream in streams:
        row = []
        for header in headers.keys():
            row.append(stream[header])
        csv.writer(f).writerow(row)


