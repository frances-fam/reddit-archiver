

import os
import sqlite3
from statistics import median
from pprint import pprint
from pymediainfo import MediaInfo
from datetime import timedelta


file_dir = "/mnt/n/all_media/frances-archive/"

dbcon = sqlite3.connect('database.sqlite')
dbcur = dbcon.cursor()

res = dbcur.execute(f"""select filename
                    from video_list where is_downloaded=1;
                    """).fetchall()

for row in res:
    filename = row[0]
    file = file_dir + filename
    try:
        info = MediaInfo.parse(file)
    except:
        dbcur.execute(f"""update video_list set is_downloaded=0 where filename='{filename}'""")
    
    file_size = info.general_tracks[0].file_size
    vid_len =0
    aud_len =0
    for track in info.tracks:
        if track.track_type =='Video':
            vid_len = track.duration
        if track.track_type == 'Audio':
            aud_len = track.duration
    
    len_diff = abs(vid_len-aud_len)
    length_min = timedelta(seconds=int(vid_len/1000))
    if len_diff>=500:
        print(f"file {filename}'s {vid_len} does not match {aud_len}, difference of : {len_diff}, marking as funky.")
        dbcur.execute(f"""update video_list set length_issue=1, length_diff={len_diff}, 
                      length='{length_min}', file_size={file_size} where filename='{filename}'""")
        dbcon.commit()
    else:
        print(f"{filename} probably ok (difference of {len_diff})")
        dbcur.execute(f"""update video_list set length_issue=0, length_diff={len_diff}, 
                      length='{length_min}', file_size={file_size} where filename='{filename}'""")
        dbcon.commit()

        