Script to download all of user Singthedamnsong's rpan streams

Requires this:
https://ffmpeg.org/download.html for the second file

Includes two files:
01_get_rpan_video_info_json.py <-- generates a json file of all of the rpan streams for user
02_download_streams_mp4.py <-- using the above file (can shorten it if you want only specific downloads)

## Todo:
Get cloud storage service to upload the files to

**Notes:** 171 streams. Assuming one hour is 500mb and each stream is 1hr to 1hr 30 in length, napkin math suggests total storage requirement would be 85-130GB to start

## Future development:
Maybe archival page with permalink to stream and also a download link

