import json
import csv
import sqlite3
import datetime

def convert_csv(username):
    filename = f'rpan_list_{username}'
    file = f'./data/{filename}.json'

    with open(file,'r') as f:
        streams = json.load(f)
        
    headers = {
        'title': 'Stream Title',
        'created_utc_timestamp' : 'Date Streamed UTC',
        'permalink' : 'Permalink',
        'stream_url': 'Stream URL',
        'username' : 'Username'
        }

    filename = f"rpan_list_{username}.json"
    file_dir = '/mnt/c/projects/frances/'
    csv_loc = f'{file_dir}/{filename}.csv'

    with open(csv_loc,'w',encoding='utf-8',newline='\n') as f:
        writer = csv.DictWriter(f,delimiter=',',fieldnames = headers.keys() ,quoting=csv.QUOTE_NONNUMERIC)
        writer.writeheader()
        for stream in streams:
            # print(stream)
            # exit()
            row = []
            for header in headers.keys():
                row.append(stream[header])
            csv.writer(f).writerow(row)

usernames =['eponamom','toyya','singthedamnsong']

for username in usernames:
    convert_csv(username)
    
    #     title_url = f"""`{row[0].replace('&amp;','&')} <https://www.reddit.com{row[1]}>`__"""
    # download_url = f"""`download link{"*" if row[7]==1 else ""} <{endpoint}{row[4]}>`__"""
    # created_utc = row[2]
    
    # is_downloaded=row[5]
    # is_uploaded=row[6]
    # streams.append({
    #     'title': title_url,
    #     'created_utc_timestamp': created_ts,
    #     'length': row[3],
    #     'download_url': download_url if is_uploaded==1 else 'tbd',
    # })
